**O(objective)**:

learn Redux. It is like pinia or vuex. Can store data in react. Without redux, we have to deliver the data from the parent component to sub component. When the project is complicated, the code will be hard to read and hard to extend. By using redux, every component can get or store data from a global store, it can greatly facilitates our programming. By practice whole day, I can use redux well. The function which is hard to accomplish yesterday is easy by using redux.
<br><br>**R(Reflective)**:happy
<br><br>**I(Interpretive)**:Using global store will greatly help us simplify the code, make code easy to write and read.
<br><br>**D(Decisional)**:Practice redux more by practice and project. And learn more about css style and layout.