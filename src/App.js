import './App.css';
import TodoList from "./components/TodoList";

function App() {
  return (
      <div className="App">
        <TodoList className="TodoItem"/>
      </div>
  );
}

export default App;
