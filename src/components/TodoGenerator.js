import {useState} from "react";
import {useDispatch} from "react-redux";
import {addToList} from "./todoListSlice";

const TodoGenerator = () => {
    const dispatch = useDispatch();
    const [input, setInput] = useState("")

    const addTodoList = () => {
        if(input!==null||input!=='') {
            dispatch(addToList(input))
        }
    }

    function updateInput(event) {
        setInput(event.target.value)
    }

    return (
        <div>
            <input type={"text"} value={input} onChange={updateInput}/>
            <button onClick={addTodoList} className='button'>add</button>
        </div>
    )

}

export default TodoGenerator;