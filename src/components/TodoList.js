import TodoGroup from "./TodoGroup";
import TodoGenerator from "./TodoGenerator";

const TodoList = () => {
    return (
        <div style={{border:"1px blue solid",padding:"10px"}}>
            <h3>Todo List</h3>
            <TodoGroup/>
            <TodoGenerator/>
        </div>
    )

}

export default TodoList;