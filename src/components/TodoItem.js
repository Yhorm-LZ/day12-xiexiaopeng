import {useDispatch} from "react-redux";
import {deleteItem, switchDoneStatus} from "./todoListSlice"

const TodoItem = ({value}) => {
    const dispatch = useDispatch();
    const deleteTodoItem = () => {
        dispatch(deleteItem(value.id))
    }
    const switchStatus = () => {
        dispatch(switchDoneStatus(value.id))
    }

    return (
        <div className="TodoItem">
            <label onClick={switchStatus} style={{minWidth: "80%", textAlign: "left"}}>
                {value.done ? <del>{value.text}</del> : value.text}
            </label>
            <button onClick={deleteTodoItem}>X</button>
        </div>
    )
}

export default TodoItem;