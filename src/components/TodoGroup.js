import TodoItem from "./TodoItem";
import {useSelector} from 'react-redux'

const TodoGroup = () => {
    const toDoList = useSelector(state => state.todoList.todoList)
    return (
        <div className="TodoGroup">
            {
                toDoList.map((value) => <TodoItem key={value.id} value={value}/>)
            }
        </div>
    )

}

export default TodoGroup;