import {createSlice, nanoid} from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
    name: 'todoList',
    initialState: {
        todoList:[],
    },
    reducers: {
        addToList:(state, action)=>{
            if(action.payload!==null||action.payload!=='') {
                const todoItem = {
                    id: nanoid(),
                    text: action.payload,
                    done: false
                }
                state.todoList.push(todoItem)
            }
        },
        switchDoneStatus:(state, action)=>{
            state.todoList.map((todoItem)=>{
                if(todoItem.id===action.payload){
                    todoItem.done=!todoItem.done
                }
                return todoItem
            })
        },
        deleteItem:(state, action)=>{
            const index = state.todoList.findIndex(item=>item.id===action.payload);
            state.todoList.splice(index,1)
        },
    }
})

export default todoListSlice.reducer
export const {addToList,switchDoneStatus,deleteItem} = todoListSlice.actions