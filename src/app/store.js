import { configureStore } from '@reduxjs/toolkit'
import todoListReducer from "../components/todoListSlice";

export default configureStore({
    reducer: {
        todoList: todoListReducer
    }
})